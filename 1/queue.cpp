#include "queue.h"
#include <iostream>

/*
	initialize an empty queue
	input: q - a queue, size - max size of the queue cells
	output: none
*/
void initQueue(queue* q, unsigned int size)
{
	int i = 0;

	q->qu = new unsigned int[size];

	for (i = 0; i < size; i++)
	{
		q->qu[i] = EMPTY_CELL; //setting all cells to empty_cell
	}
	
	q->length = 0; //no activated cells right now
	q->maxSize = size;
}

/*
	clears a queue (frees it)
	input: pointer to a queue
	output: none
*/
void cleanQueue(queue* q)
{
	delete q->qu;
	delete q;
}

/*
	removes a value from queue
	input: pointer to the queue
	output: the value that got removed

*/
int dequeue(queue* q)
{
	int removed = 0;
	
	if (q->length != 0)
	{
		removed = q->qu[q->length - 1];
		q->qu[q->length - 1] = EMPTY_CELL;
		q->length--;
	}
	else
	{
		removed = -1; //returns -1 if queue is empty
	}
	
	return removed;
}

/*
	the function print the queue
	input: pointer to queue
	output: none
*/
void printQueue(queue* q)
{
	int i = 0;

	std::cout << "-> ";

	for (i = 0; i < q->length; i++)
	{
		std::cout << q->qu[i] << ", ";
	}

	std::cout << "\n";
}

/*
	the function adds a value to the queue
	input: q - the queue, newValue - new value to add to the list
	output: none 
*/
void enqueue(queue* q, unsigned int newValue)
{

	if (q->length != q->maxSize) //making sure there's a room for another number
	{
		shiftArrayRight(q->qu, q->maxSize); //moving all right
		q->qu[0] = newValue; //inserting the new value
		q->length++;
	}
}

/*
	the function shift an array of unsigned ints to the right
	*assuming the last inedx of the array can be deleted
	input: the array and it's length
	output:none
*/
void shiftArrayRight(unsigned int* arr, int arrLength)
{
	int i = 0;

	for (i = arrLength - 1; i > 0; i--)
	{
		arr[i] = arr[i - 1];
	}

}
