#ifndef QUEUE_H
#define QUEUE_H

#define EMPTY_CELL NULL


/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* qu;
	unsigned int length;
	unsigned int maxSize;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

void printQueue(queue* q);
void shiftArrayRight(unsigned int* arr,int arrLength);


#endif /* QUEUE_H */