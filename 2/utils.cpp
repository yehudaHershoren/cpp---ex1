#include "utils.h"
#include "stack.h"
#include <cstdio>
#include <iostream>


/*
	the function inputs 10 ints from the user into an array, and returns a reversed array
	input: none
	output: the pointer to the reversed array
*/
int* reverse10()
{
	int* arr = new int[INPUT_NUM];
	int i = 0;

	for (i = 0; i < INPUT_NUM; i++)
	{
		std::cout << "enter value for cell " << i << ": ";
		std::cin >> arr[i];
	}

	reverse(arr,INPUT_NUM);

	return arr;
}



/*
	the function reverse the order of an array
	input: array and it's size
	output: none
*/
void reverse(int* nums, unsigned int size)
{
	stack* st = new stack();
	int i = 0;
	int popValue = 0;

	if (nums != NULL) // if array isn't empty
	{

		for (i = 0; i < size; i++)
		{
			push(st, nums[i]); //pushing all array values into a stack
		}

		i = 0; //reseting index

		while (popValue != STACK_EMPTY) //while stack is not empty
		{
			popValue = pop(st); //popping
			nums[i] = popValue; //applying value into array
			i++;
		}
	}
	
}


