#include "linkedList.h"
#include <cstdio>
#include <iostream>

/*
	the function removes the first node in the list
	input: a pointer-to-pointer to a linked list 
	output: pointer to the node that got removed
*/
linkedList* removeNode(linkedList** lst)
{
	linkedList* removed = 0;
	removed = *lst; //saving node
	if (*lst != NULL)
	{
		*lst = (*lst)->next; //deleting node
	}

	return removed;
}



/*
	the function adds a node in the beggining of the linked list
	input: pointer-to-pointer to a linked list
	output:none
*/
void addNode(linkedList** lst, unsigned int newValue)
{
	linkedList* newNode = new linkedList(); //creating new node
	newNode->value = newValue; 
	newNode->next = *lst; 

	*lst = newNode; //adding to the beggining
}

/*
	the function prints a linked list
	input: a linked list
	output: none
*/
void printList(linkedList* lst)
{
	std::cout << "-> ";

	while (lst != NULL)
	{
		std::cout << lst->value << ", ";
		lst = lst->next;
	}
	std::cout << "\n";
}

/*
	the function deletes a linked list
	input: a pointer-to-pointer to a linked list
	output: none
*/
void deleteList(linkedList** lst)
{
	linkedList* p = *lst;
	linkedList* nextNode = 0;

	if (p == NULL) //if list is empty
	{
		delete p;
	}
	else
	{
		nextNode = p->next;
		
		while (nextNode != NULL) // until we not longer have a next
		{
			delete p; //deleting previouse node
			p = nextNode;
			nextNode = p->next; //moving to the next one
		}
		delete p; // deleting last node
	}
}
