#pragma once


typedef struct linkedList
{
	unsigned int value;
	linkedList* next;

}linkedList;

linkedList* removeNode(linkedList** lst);
void addNode(linkedList** lst, unsigned int newValue);
void printList(linkedList* lst);

void deleteList(linkedList** lst);
