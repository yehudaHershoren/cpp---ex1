#include <cstdio>
#include "stack.h"
#include "linkedList.h"

/*
	the function initialize a stack
	input: a pointer to the stack
	output: none
*/
void initStack(stack* s)
{
	
	linkedList* tmp = NULL;
	s->list = tmp;
}

/*
	the function cleans (free) the stack
	input: pointer to the stack
	output: none
*/
void cleanStack(stack* s)
{
	deleteList(&(s->list));
}

/*
	the function push (adds) a new element to the stack
	input:	s - a pointer to the stack
			element - the new element to add		
*/
void push(stack* s, unsigned int element)
{
	addNode(&(s->list), element);
}

/*
	the function removes the top element in the stack and returns it's value
	input: a pointer to the stack
	output:	stack is empty = -1
			else = the value of the removed element
				
*/
int pop(stack* s) // Return -1 if stack is empty
{
	linkedList* removed = 0;
	int result = 0;

	removed = removeNode(&(s->list)); //getting the removed node
	
	if (removed == NULL) // stack is empty
	{
		result = STACK_EMPTY;
	}
	else
	{
		result = removed->value;
	}
	return result;
}

